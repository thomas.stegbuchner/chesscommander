# tsteg userinterfaces 2020 - 04.05.2020

print(" --- Welcome to Chesscommander Pre Alpha ---")

# LIB IMPORTS
# from func import * 
from graphics import *

EMPTY = 0
W_KING = 1
W_QUEEN = 2
W_ROOK = 3
W_BISHOP = 4
W_KNIGHT = 5
W_PAWN = 6
B_KING = 7
B_QUEEN = 8
B_ROOK = 9
B_BISHOP = 10
B_KNIGHT = 11
B_PAWN = 12

WAIT_FOR_GAMESTART = 1
PLAYER_A_TURN = 2
PLAYER_B_TURN = 3

def reset_game():
	gamefield.extend([
		W_ROOK, 	W_KNIGHT, 	W_BISHOP, 	W_QUEEN, 	W_KING, 	W_BISHOP, 	W_KNIGHT, 	W_ROOK,
		W_PAWN,		W_PAWN,		W_PAWN,		W_PAWN,		W_PAWN,		W_PAWN,		W_PAWN,		W_PAWN,
		EMPTY,		EMPTY,		EMPTY,		EMPTY,		EMPTY,		EMPTY,		EMPTY,		EMPTY,
		EMPTY,		EMPTY,		EMPTY,		EMPTY,		EMPTY,		EMPTY,		EMPTY,		EMPTY,
		EMPTY,		EMPTY,		EMPTY,		EMPTY,		EMPTY,		EMPTY,		EMPTY,		EMPTY,
		EMPTY,		EMPTY,		EMPTY,		EMPTY,		EMPTY,		EMPTY,		EMPTY,		EMPTY,
		B_PAWN,		B_PAWN,		B_PAWN,		B_PAWN,		B_PAWN,		B_PAWN,		B_PAWN,		B_PAWN,
		B_ROOK, 	B_KNIGHT, 	B_BISHOP, 	B_QUEEN, 	B_KING, 	B_BISHOP, 	B_KNIGHT, 	B_ROOK,
	])
	
	
def getPieceAtCoord(x,y):
	if(x >= "a" and x <= "h" and y >= 1 and y <= 8):
		print("valid")
	else:
		print("invalid")

#x is a,b,c,d,e,f,g,h
#y is 1,2,3,4,5,6,7,8

def draw(what_to_draw):
	if(what_to_draw == "gamefield_bg"):
		print("print gamefield")
	if(what_to_draw == "chess_pieces"):
		print("no")

offset_x = 10
offset_y = 25

def drawGameBG():
	i = 0
	while i < 64:
		x = i%8
		y = int(i/8)
		
		if((y%2 == 0 and x%2 == 0)or(y%2 == 1 and x%2 == 1)): 
			rect = Rectangle(Point(offset_x+x*10, offset_y+y*10), Point(offset_x+x*10+10, offset_y+y*10+10))
			rect.setFill('darkgrey')
			rect.draw(win)
		else: 
			rect = Rectangle(Point(offset_x+x*10, offset_y+y*10), Point(offset_x+x*10+10, offset_y+y*10+10))
			rect.setFill('white')
			rect.draw(win)
		i = i + 1

def drawLegend():
	i = 1
	while i <= 8: #vertical
		text = Text(Point((3*offset_x/4), offset_y-5+i*10), str(i))
		text.draw(win)
		i = i + 1
	
	i = 1 #= ascii a
	while i <= 8: #horizontal
		text = Text(Point(offset_x-5+i*10, offset_y-2), chr(i+96))
		text.draw(win)
		i = i + 1

	text_legend = Text(Point(offset_x + 100,offset_y + 47), "Speech Commands:\n\nNEW GAME")
	text_legend.draw(win)
		
def drawAllPieces():
	i = 0
	while i < 64:
		x = i%8
		y = int(i/8)
		
		if(gamefield[i] != EMPTY and gamefield[i] == W_KING):
			pic = Image(Point(offset_x+5+x*10, offset_y+5+y*10), "img/W_KING.png").draw(win)
	
		if(gamefield[i] != EMPTY and gamefield[i] == W_QUEEN):
			pic = Image(Point(offset_x+5+x*10, offset_y+5+y*10), "img/W_QUEEN.png").draw(win)
		
		if(gamefield[i] != EMPTY and gamefield[i] == W_ROOK):
			pic = Image(Point(offset_x+5+x*10, offset_y+5+y*10), "img/W_ROOK.png").draw(win)
		
		if(gamefield[i] != EMPTY and gamefield[i] == W_BISHOP):
			pic = Image(Point(offset_x+5+x*10, offset_y+5+y*10), "img/W_BISHOP.png").draw(win)
		
		if(gamefield[i] != EMPTY and gamefield[i] == W_KNIGHT):
			pic = Image(Point(offset_x+5+x*10, offset_y+5+y*10), "img/W_KNIGHT.png").draw(win)
			
		if(gamefield[i] != EMPTY and gamefield[i] == W_PAWN):
			pic = Image(Point(offset_x+5+x*10, offset_y+5+y*10), "img/W_PAWN.png").draw(win)
		
		
		if(gamefield[i] != EMPTY and gamefield[i] == B_KING):
			pic = Image(Point(offset_x+5+x*10, offset_y+5+y*10), "img/B_KING.png").draw(win)
	
		if(gamefield[i] != EMPTY and gamefield[i] == B_QUEEN):
			pic = Image(Point(offset_x+5+x*10, offset_y+5+y*10), "img/B_QUEEN.png").draw(win)
		
		if(gamefield[i] != EMPTY and gamefield[i] == B_ROOK):
			pic = Image(Point(offset_x+5+x*10, offset_y+5+y*10), "img/B_ROOK.png").draw(win)
		
		if(gamefield[i] != EMPTY and gamefield[i] == B_BISHOP):
			pic = Image(Point(offset_x+5+x*10, offset_y+5+y*10), "img/B_BISHOP.png").draw(win)
		
		if(gamefield[i] != EMPTY and gamefield[i] == B_KNIGHT):
			pic = Image(Point(offset_x+5+x*10, offset_y+5+y*10), "img/B_KNIGHT.png").draw(win)
			
		if(gamefield[i] != EMPTY and gamefield[i] == B_PAWN):
			pic = Image(Point(offset_x+5+x*10, offset_y+5+y*10), "img/B_PAWN.png").draw(win)
		
		#print('x is' , x , 'y is' , y)
		
		i = i + 1

def gameLoop():
	while True:
		text_out.setText(text_in.getText())
		
		
# BASIC SETUP
win = GraphWin(width = 1120, height = 960) #create a window
win.setCoords(0, 0, 140, 120) #set coords


# GRAPHICS OBJECTS CREATION #
gamefieldRect = Rectangle(Point(20, 40), Point(80, 100)) #main gamefield
gamefieldRect.draw(win) # draw it to the window

text_in = Entry(Point(offset_x + 100,offset_y + 77),10)
text_in.draw(win)
text_out = Text(Point(offset_x + 100,offset_y + 67), "")
text_out.draw(win)

# DRAW ROUTINE #

gamefield = [EMPTY] * 0
playerAgrave = []
playerBgrave = []

reset_game()

print(gamefield)


drawGameBG()
drawLegend()
drawAllPieces()

gameLoop()

win.getMouse() # pause before closing
win.close()
